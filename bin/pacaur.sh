buildroot="$(mktemp -d)"

# Fetch Dave Reisner's key to be able to verify cower.
gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 487EACC08557AD082088DABA1EB2638FF56C0C53

# Make sure we can even build packages on arch linux.
sudo pacman -S --needed --noconfirm base-devel git

mkdir -p "$buildroot"
cd "$buildroot" || exit 1

git clone "https://aur.archlinux.org/cower.git"
git clone "https://aur.archlinux.org/pacaur.git"

cd "${buildroot}/cower" || exit 1
makepkg --syncdeps --install --noconfirm

cd "${buildroot}/pacaur" || exit 1
makepkg --syncdeps --install --noconfirm

cd "$HOME" || exit 1
rm -rf "$buildroot"
