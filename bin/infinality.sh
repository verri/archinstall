PKG="infinality-bundle ttf-bh-ib ttf-caladea-ib ttf-cantoraone-ib ttf-carlito-ib
ttf-courier-prime-ib ttf-ddc-uchen-ib ttf-dejavu-ib ttf-droid-ib ttf-faruma-ibx
ttf-gelasio-ib ttf-hack-ibx ttf-heuristica-ib ttf-inknut-antiqua-ibx ttf-liberastika-ib
ttf-liberation-ib ttf-merriweather-ib ttf-merriweather-sans-ib ttf-monoid-ibx
ttf-noto-fonts-ib ttf-opensans-ib ttf-symbola-ib ttf-ubuntu-font-family-ib ttf-source-code-pro-ibx"

sudo pacman -S --needed $PKG

date >> .infinality
