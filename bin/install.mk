.ufw: ufw.sh
	sh ufw.sh

.reflector: reflector.sh
	sh reflector.sh

.preload: .pacaur preload.sh
	sh preload.sh

.gtk: gtk.sh ../home/.config/gtk-3.0/gtk.css ../home/.config/gtk-3.0/settings.ini
	sh gtk.sh

.psd: .pacaur psd.sh
	sh psd.sh

.firefox: .gtk .pacaur .psd .gnomekeyring firefox.sh
	sh firefox.sh

.laptop: laptop.sh
	sh laptop.sh

.neovim: neovim.sh ../home/.config/nvim/init.vim
	sh neovim.sh

.zsh: zsh.sh ../home/.zshrc
	sh zsh.sh

.xorg: xorg.sh
	sh xorg.sh

.i3: .infinality .networkmanager .twmn .termite .i3pystatus .locker i3.sh ../home/.config/i3/config ../home/.xinitrc ../home/.zlogin
	sh i3.sh

.twmn: twmn.sh
	sh twmn.sh

.infinality: .pacman .xorg infinality.sh
	sh infinality.sh

.networkmanager: .gnomekeyring networkmanager.sh
	sh networkmanager.sh

.gnomekeyring: gnomekeyring.sh
	sh gnomekeyring.sh

.pacaur: .pacman
	sh pacaur.sh
	date >> .pacaur

.volume: .twmn volume.sh
	sh volume.sh

.termite: .infinality
	sh termite.sh

.i3pystatus: .volume .pacaur .gsimplecal i3pystatus.sh
	sh i3pystatus.sh

.gsimplecal: gsimplecal.sh
	sh gsimplecal.sh

.locker: locker.sh
	sh locker.sh

.pacman: pacman.sh
	sh pacman.sh
