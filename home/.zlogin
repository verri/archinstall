if [[ -z $DISPLAY && $XDG_VTNR -eq 1 ]]; then
    systemctl --user start lightson.timer
    startx -- -keeptty -nolisten tcp > /tmp/xorg.log 2>&1
    exec systemctl --user stop lightson.timer
fi
return 0
