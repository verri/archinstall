source ./utils.sh

sudo pacman -S --needed --noconfirm gnome-keyring libgnome-keyring openssh

rootcpkup ../root/etc/pam.d/login /etc/pam.d/login
rootcpkup ../root/etc/pam.d/passwd /etc/pam.d/passwd

echo "Start SSH and Secrets components of keyring daemon"
mkdir -p $HOME/.config/autostart
cp /etc/xdg/autostart/{gnome-keyring-secrets.desktop,gnome-keyring-ssh.desktop} $HOME/.config/autostart/
sed -i '/^OnlyShowIn.*$/d' $HOME/.config/autostart/gnome-keyring-secrets.desktop
sed -i '/^OnlyShowIn.*$/d' $HOME/.config/autostart/gnome-keyring-ssh.desktop

echo "Integrate with HTTPS Git"
cd /usr/share/git/credential/gnome-keyring
sudo make
cd -
git config --global credential.helper /usr/lib/git-core/git-credential-gnome-keyring

date >> .gnomekeyring
