source ./utils.sh

echo "Installing zsh..."
sudo pacman -S --needed --noconfirm zsh zsh-completions thefuck

echo "Installing oh my zsh..."
git clone git://github.com/robbyrussell/oh-my-zsh.git ${HOME}/.oh-my-zsh

echo "Copying configuration files..."
for FILE in .zshrc
do
    cpkup ../home/${FILE} ${HOME}/${FILE}
done

date >> .zsh
