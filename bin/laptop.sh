source ./utils.sh

# Not working, discharging events are never sent.
# rootcpkup ../root/etc/udev/rules.d/99-lowbat.rules /etc/udev/rules.d/99-lowbat.rules

sudo pacman -S --needed --noconfirm tlp
sudo systemctl disable systemd-rfkill.service
sudo systemctl enable tlp.service tlp-sleep.service

date >> .laptop
