cpkup() {
    if test -e $2; then
        mkdir -p ../bkp/"$(dirname $2)"
        cp $2 ../bkp/"$(dirname $2)"/
    fi
    mkdir -p $(dirname $2)
    cp $1 $2
}

rootcpkup() {
    if test -e $2; then
        mkdir -p ../bkp/"$(dirname $2)"
        cp $2 ../bkp/"$(dirname $2)"/
    fi
    sudo mkdir -p $(dirname $2)
    sudo cp $1 $2
}

