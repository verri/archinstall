source ./utils.sh

echo "Installing neovim and dependencies..."
sudo pacman -S --needed --noconfirm neovim python2-neovim xsel
cpkup {../home,$HOME}/.config/nvim/init.vim
cpkup {../home,$HOME}/.ycm_extra_conf.py

echo "Installing Vundle..."
mkdir -p $HOME/.config/nvim/bundle
git clone https://github.com/VundleVim/Vundle.vim.git $HOME/.config/nvim/bundle/Vundle.vim

echo "Installing plugins..."
nvim +PluginInstall +qall

date >> .neovim
