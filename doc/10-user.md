# User management

Steps when booting for the first time.

## Creating the user

Create the first user (USER) and set its password.

```
# useradd -m -g users -G wheel -s /bin/zsh USER
# passwd USER
```

## Privilege escalation

Run:

```
# visudo
```

And uncomment the following line:

```
## Uncomment to allow members of group wheel to execute any command
%wheel ALL=(ALL) ALL
```