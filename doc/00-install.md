# Installing the system

Steps in the installation media.

## UEFI

To verify you are in UEFI mode, check that the following directory is
populated:

```
# ls /sys/firmware/efi/efivars
```

## Keyboard layout

If using a Brazilian keyboard:

```
# loadkeys br-abnt2
```

## Connect to the Internet

Let WLAN and ETH be the wireless and wired interfaces respectively.
Usually:

```
WLAN = wlan0, wlp0s0
ETH = eno0, enp0s0f0
```

### Wireless

```
# wifi-menu -o wlan0
```

### Wired

```
# dhcpcd ETH
```

## Prepare the storage devices

### Identify the devices

```
$ lsblk
```

In the following sections, consider SSD the path to the SSD.
Usually:

```
SSD = sda, nvme0n1p
```

### Partitioning

If using SSD, it is a good pratice to keep 25% of free space for
garbage collection.

Create the partitions using gdisk:

```
# gdisk /dev/SSD
```

```
PARTITION       SIZE        TYPE
Partition 1     200M        ef00 EFI System
Partition 2     100%        8e00 Linux LVM
```

### Setup LVM

Create the LVM groups and volumes.
It is a good pratice to use the computer name
(COMPUTERNAME) for the volume group.
Considering a 128G SSD, run:

```
# pvcreate /dev/SSD2
# vgcreate COMPUTERNAME /dev/SSD2
# lvcreate -L 32G COMPUTERNAME -n root
# lvcreate -L 64G COMPUTERNAME -n home
```

Format the partitions:

```
# mkfs.fat -F32 -n BOOT /dev/SSD1
# mkfs.ext4 -L ROOT /dev/mapper/COMPUTERNAME-root
# mkfs.ext4 -L HOME /dev/mapper/COMPUTERNAME-home
```

Mount the partitions:

```
# mount /dev/mapper/COMPUTERNAME-root /mnt
# mkdir /mnt/{boot,home}
# mount /dev/disk/by-label/BOOT /mnt/boot
# mount /dev/mapper/COMPUTERNAME-home /mnt/home
```

## System installation

Bring the fastest/nearest mirrors on top of the file:

```
# nano /etc/pacman.d/mirrorlist
```

Install the basic packages:

```
# pacstrap -i /mnt base base-devel vim zsh git dialog wpa_supplicant intel-ucode ifplugd
```

## Basic configuration

Generate fstab:

```
# genfstab /mnt | sed 's/relatime/noatime/' > /mnt/etc/fstab
```

Change root:

```
# arch-chroot /mnt /bin/bash
```

### Locale and keyboard

Uncomment en_US.UTF-8 locale in /etc/locale.gen and run:

```
# locale-gen
```

Set the locale options:

```
/etc/locale.conf

LANG=en_US.UTF-8
```

Set the keyboard layout (if using Brazilian keyboard):

```
/etc/vconsole.conf

KEYMAP=br-abnt2
```

Select time zone:

```
# ln -s /usr/share/zoneinfo/ZONE/SUBZONE /etc/localtime
```

Adjust hardware clock and enable ntp:

```
# hwclock --systohc --utc
# timedatectl set-ntp true
```

### Prepare boot

Adjust HOOKS and MODULES in /etc/mkinitcpio.conf:

```
MODULES="intel_agp i915" # for Intel video cards
HOOKS="base systemd autodetect modconf block sd-lvm2 filesystems keyboard fsck"
```

Alternatively, if boot or hibernation errors occurs use:

```
HOOKS="base udev autodetect modconf block lvm2 resume filesystems keyboard fsck"
```

And run:

```
# mkinitcpio -p linux
```

Create a swap file and give to it low priority (pri=10):

```
# fallocate -l 4G /swapfile
# chmod 600 /swapfile
# mkswap /swapfile
# swapon /swapfile
# echo "/swapfile none swap defaults,pri=10 0 0" >> /etc/fstab
```

Reduce swappiness to increase responsiveness:

```
/etc/sysctl.d/99-sysctl.conf

vm.swappiness=10
```

Recover the value of swap offset running:

```
# filefrag -v /swapfile
```

The output is in a table format and the required value is
located in the first row of the physical_offset column.
In the following, RESUME_OFFSET refers to the offset value.

Install boot loader at the EFI partition:

```
# bootctl install
```

Create a boot entry:

```
/boot/loader/entries/linux.conf

title          Linux
linux          /vmlinuz-linux
initrd         /intel-ucode.img
initrd         /initramfs-linux.img
options        root=/dev/mapper/COMPUTERNAME-root rw rootflags=noatime,data=ordered quiet resume=/dev/mapper/COMPUTERNAME-root resume_offset=RESUME_OFFSET zswap.enabled=1
```

Select the default entry, zero timeout and disable the editor (for
security reasons):

```
/boot/loader/loader.conf

timeout 0
default linux
editor  0
```

### Networking

Set the hostname:

```
/etc/hostname

COMPUTERNAME
```

Enable the networking services:

```
# systemctl enable netctl-auto@WLAN.service
# systemctl enable netctl-ifplugd@ETH.service
```

### Finishing

Set the root password, unmount the partitions and reboot:

```
# passwd
# swapoff /swapfile
# exit
# umount -R /mnt
# reboot
```
