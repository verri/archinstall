source ./utils.sh

sudo pacman -S --needed --noconfirm xautolock libnotify

if test ! -e $HOME/.local/share/i3lock; then
    mkdir -p $HOME/.local/share
    mkdir -p $HOME/.local/bin
    git clone https://github.com/verri/i3lock.git $HOME/.local/share/i3lock
    cd $HOME/.local/share/i3lock
    make
    mv i3lock $HOME/.local/bin/locker
    cd -
fi

rootcpkup {../root,/}/etc/systemd/system/lock-before-sleep.service
sudo systemctl enable lock-before-sleep.service

date >> .locker
