source ./utils.sh

pacaur -S --needed --noconfirm i3-gaps rofi compton xdotool xorg-xprop feh

cpkup ../home/.config/i3/config $HOME/.config/i3/config
cpkup ../home/.config/rofi/config $HOME/.config/rofi/config
cpkup ../home/.xinitrc $HOME/.xinitrc
cpkup ../home/.zlogin $HOME/.zlogin

cpkup {../home,$HOME}/.config/systemd/user/lightson.timer
cpkup {../home,$HOME}/.config/systemd/user/lightson.service
cpkup {../home,$HOME}/.local/bin/lightson
chmod +x $HOME/.local/bin/lightson

cpkup {../home,$HOME}/.local/bin/wallpaper
chmod +x $HOME/.local/bin/wallpaper

date >> .i3
