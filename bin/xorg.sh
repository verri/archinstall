source ./utils.sh

PKG="xorg-server xf86-video-intel xf86-input-libinput xorg-xinit xorg-xsetroot"

echo Installing "$PKG" ...
sudo pacman -S --needed --noconfirm $PKG

sudo localectl set-x11-keymap br,gr microsoft , "grp:shifts_toggle,terminate:ctrl_alt_bksp,caps:escape"

rootcpkup ../root/etc/X11/xorg.conf.d/30-touchpad.conf /etc/X11/xorg.conf.d/30-touchpad.conf

date >> .xorg
