"  -------------------------------------
" Vundle
" -------------------------------------
filetype off

set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin("~/.config/nvim/bundle/")

Plugin 'gmarik/Vundle.vim'
Plugin 'bling/vim-airline'
Plugin 'Valloric/YouCompleteMe'
" Plugin 'fatih/vim-go'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'jiangmiao/auto-pairs'
Plugin 'airblade/vim-gitgutter'
" Plugin 'vim-pandoc/vim-pandoc-syntax'
" Plugin 'vim-pandoc/vim-pandoc'
" Plugin 'vim-pandoc/vim-rmarkdown'
" Plugin 'editorconfig/editorconfig-vim'

call vundle#end()
filetype plugin indent on


" -------------------------------------
" Configuration
" -------------------------------------
autocmd BufWritePre * :%s/\s\+$//e " remove trailing spaces
syntax on
set bg=dark
set modeline
set title
set ruler
set clipboard+=unnamedplus " X clipboard
set cpo+=J " double space after period
set nobackup " turn backup *~ files
set nowrap
set enc=utf-8
set fenc=utf-8 " set (force) UTF-8 encoding
set termencoding=utf-8
set autoindent " use indentation of previous line
set smartindent " use intelligent indentation for C
set smartcase
set smarttab
set tabstop=2
set shiftwidth=2
set expandtab
set textwidth=90
set number
set showmatch
set comments=sl:/*,mb:\ *,elx:\ */ " intelligent comments
set mouse=a
set wildmenu
set incsearch
set list listchars=tab:\→\ ,trail:·
set backspace=indent,eol,start
set nohlsearch
set sidescroll=1
set inccommand=split

" " -------------------------------------
" " Shortcuts
" " -------------------------------------
nmap <F2> :w<CR>
imap <F2> <ESC>:w<CR>
nmap <F3> :YcmDiags<CR>
imap <F3> <ESC>:YcmDiags<CR>
nmap <F5> :silent execute "!make&>/dev/null &" \| redraw!<CR>
imap <F5> <ESC> :silent execute "!make&>/dev/null &" \| redraw!<CR>
tnoremap <ESC> <C-\><C-n>
" nmap <F6> :Dox<CR>
" imap <F6> <ESC>:Dox<CR>
" map <C-F12> :!ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q .<CR>


" -------------------------------------
" Spell
" -------------------------------------
" setlocal spell spelllang=pt,en
" setlocal spellfile=$HOME/.vim/spell/pt.utf-8.add,$HOME/.vim/spell/en.utf-8.add
highlight clear SpellBad
highlight SpellBad term=reverse cterm=underline


" -------------------------------------
" YouCompleteMe
" -------------------------------------
let g:ycm_confirm_extra_conf = 0
let g:ycm_complete_in_comments = 1
let g:ycm_collect_identifiers_from_comments_and_strings = 1
let g:ycm_filepath_completion_use_working_dir = 1
let g:ycm_always_populate_location_list = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
map <C-f> :YcmCompleter FixIt<CR>
" let g:ycm_filetype_blacklist = {}

" -------------------------------------
" Airline
" -------------------------------------
" let g:airline#extensions#tabline#enabled = 1
" let g:airline_powerline_fonts = 1
let g:airline_left_sep=''
let g:airline_right_sep=''

" " -------------------------------------
" " Auto Pairs
" " -------------------------------------
let g:AutoPairsShortcutBackInsert = ''

" " -------------------------------------
" " Git
" " -------------------------------------
let g:gitgutter_realtime = 1
let g:gitgutter_eager = 1
set updatetime=250
nmap hs <Plug>GitGutterStageHunk
nmap hu <Plug>GitGutterUndoHunk
nmap hp <Plug>GitGutterPreviewHunk


" -------------------------------------
" Omnicomplete
" -------------------------------------
" filetype plugin on
" " Load standard tag files
" set tags+=~/.vim/tags/cpp
" " set tags+=~/.vim/tags/gl
" " set tags+=~/.vim/tags/sdl
" " set tags+=~/.vim/tags/qt4
" " set tags+=~/.vim/tags/lua
" let OmniCpp_NamespaceSearch = 1
" let OmniCpp_GlobalScopeSearch = 1
" let OmniCpp_ShowAccess = 1
" let OmniCpp_ShowPrototypeInAbbr = 1 " show function parameters
" let OmniCpp_MayCompleteDot = 1 " autocomplete after .
" let OmniCpp_MayCompleteArrow = 1 " autocomplete after ->
" let OmniCpp_MayCompleteScope = 1 " autocomplete after ::
" automatically open and close the popup menu / preview window
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt+=menuone,menu,longest,preview


" -------------------------------------
" Visual
" -------------------------------------
hi Folded ctermfg=white ctermbg=black
hi Visual ctermfg=black ctermbg=white
hi Comment cterm=italic
hi! link Conceal Special
set foldlevelstart=99


" -------------------------------------
" File-specific Configuration
" -------------------------------------
au BufNewFile,BufRead *.n   set filetype=nayara tw=90
au BufNewFile,BufRead *.g4  set nospell filetype=antlr3
au BufNewFile,BufRead *.py  set textwidth=79
au BufNewFile,BufRead *.zy  set filetype=ritzy tw=100 foldlevelstart=0
au BufNewFile,BufRead *.mpp set filetype=cpp
au BufNewFile,BufRead *.mxx set filetype=cpp

" -------------------------------------
" Create folders if necessary
" -------------------------------------
" not working
" augroup auto_mkdir
"     autocmd!
"     autocmd BufWritePre *
"     \ if !isdirectory(expand("<afile>:p:h")) |
"         \ call mkdir(expand("<afile>:p:h"), "p") |
"     \ endif
" augroup END

" -------------------------------------
" C++ Options
" -------------------------------------
let c_no_curly_error=1
