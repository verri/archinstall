source ./utils.sh

PKG="i3pystatus python-psutil python-colour"

pacaur -S --needed --noconfirm $PKG
cpkup ../home/.config/i3/pystatus.py $HOME/.config/i3/pystatus.py

date >> .i3pystatus
