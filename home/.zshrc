zstyle ':completion:*' completer _expand _complete _ignored _match _correct _approximate _prefix
zstyle ':completion:*' completions c1
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' format '---- %B%Ucompleting: %d%u%b'
zstyle ':completion:*:warnings' format $'---- %{\e[0;31m%}no match for: %B%d%b%{\e[0m%}'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' glob 1
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' max-errors 2
zstyle ':completion:*' menu select=1
zstyle ':completion:*' original true
zstyle ':completion:*' prompt 'Number of errors: %e'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' substitute 1

zstyle :compinstall filename '/home/verri/.zshrc'

autoload -Uz compinit
compinit

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000000
setopt appendhistory autocd extendedglob
unsetopt beep nomatch notify
bindkey -v

ZSH=$HOME/.oh-my-zsh
ZSH_THEME="wedisagree"

COMPLETION_WAITING_DOTS="true"
plugins=(git git-extras history-substring-search thefuck)

source $ZSH/oh-my-zsh.sh

export WEKA_HOME=$HOME/.wekafiles
export EDITOR=nvim
export MAKEFLAGS=-j4
export PATH=$PATH:$HOME/.local/bin

alias vim="nvim"
alias 7zultra="7za a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on"

# Colored man pages
man() {
    env LESS_TERMCAP_mb=$(printf "\e[1;31m") \
	    LESS_TERMCAP_md=$(printf "\e[1;31m") \
	    LESS_TERMCAP_me=$(printf "\e[0m") \
	    LESS_TERMCAP_se=$(printf "\e[0m") \
	    LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
	    LESS_TERMCAP_ue=$(printf "\e[0m") \
	    LESS_TERMCAP_us=$(printf "\e[1;32m") \
	    man "$@"
}

# Setup termite
if [[ $TERM == xterm-termite ]]; then
  . /etc/profile.d/vte.sh
  __vte_osc7
fi
