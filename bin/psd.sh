source ./utils.sh

pacaur -S --needed --noconfirm profile-sync-daemon
cpkup {../home,$HOME}/.config/psd/psd.conf

systemctl --user enable psd.service

echo "Add the following line to the sudoers file."
echo "$(whoami) ALL=(ALL) NOPASSWD: /usr/bin/psd-overlay-helper"
read line

sudo visudo

date >> .psd
